# Gitlab Groups

## US 146078:
Es soll eine Ansible Rolle erstellt werden, die auf einem GitLab in einer bestimmten Struktur eine Gruppe erstellt. Die Struktur selber kann auf root Ebene sein oder selber wiederum unterhalb einer anderen GitLab Gruppe. Im Rahmen der Erstellung der GitLab Gruppen sollen automatisch auch jeweils 3 AD Berechtigungsgruppen erstellt werden, die jeweils je Schreib-, Lese- oder Adminberechtigungen haben. Die Namenskonvention der verlinkten AD Gruppen folgt dabei analog den Konventionen zu Nexus Repositories. Der Name der GitLab Gruppe ist hierbei aber frei für den Aufrufenden wählbar.

## AD Benutzergruppen und Benutzer

In Gitlab gibt es keine feinkörnige Rechteverwaltung wie in Nexus. Stattdessen werden sechs Rollen (+ no access) unterschieden:

Role (access level):

```
No access (0)
Minimal access (5)
Guest (10)
Reporter (20)
Developer (30)
Maintainer (40)
Owner (50)
```

AD Benutzer in Group gitlab-dcs & gitlab-<role>-group zum Testen:

```
first name: max muster
last name: guest
logon name: mmguest
pw: s3dc1234$

first name: max muster
last name: reporter
logon name: mmrep
pw: s3dc1234$

first name: max muster
last name: developer
logon name: mmdev
pw: s3dc1234$

first name: max muster
last name: owner
logon name: mmowner
pw: s3dc1234$

first name: max muster
last name: admin
logon name: mmadmin
pw: s3dc1234$
```

Gitlab Benutzergruppen und dazugehörige Benutzer:

```
gitlab-guest-group (mit mmguest)
gitlab-developer-group (mit mmdev)
gitlab-admin-group (mit mmadmin)
gitlab-owner-group (mit mmowner)
gitlab-admin-group (mit mmadmin)
```

Lists LDAP group links

```
GET /groups/:id/ldap_group_links

curl --request GET --header "PRIVATE-TOKEN: glpat-hK3pHjMPWdUuSKaSe55U" \
     "http://10.1.56.30/api/v4/groups/182/ldap_group_links"

```

