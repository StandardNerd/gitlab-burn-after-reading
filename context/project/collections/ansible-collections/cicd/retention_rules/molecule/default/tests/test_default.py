def test_gitlab_project_created(host):
    result = host.run('ansible-playbook -i "localhost," -c local playbook.yml')
    assert result.rc == 0
    assert "project_created" in result.stdout
