
# Gitlab Archivierungs- und Löschkonzept

## Quickstart

Run Tests in directory retention_rules/tests/

```
ansible-playbook pb_test_delete_group_not_deleted.yml -e "seed_projects=4"
```


## Ausgangsstellung

> Folgende externe Schnittstellen werden für GitLab automatisch zur Verfügung gestellt:
>
> Löschen von Projekten (Repositories)
> Löschen von Gruppen (Folder)
> Konfiguration von Projekten
>
>  
>
> Hypothese:
>
> - Archivierung in GitLab selbst
>
> - Archivierung entspricht read only (für bisherige) und der rest gemäss App Standardverhalten
>
> - Löschung prüft ob Retention eingehalten wurde bevor gelöscht wird
>
> - Löschung wird periodisch automatisch ausgeführt
>
>  
>
> Parameter:
>
> - Archivdauer: 6 Monate (frei definierbar, muss final vom Kunden definiert werden können)
>
> - Löschintervall: offen wer und wie die Löschung beauftragt wird (WS nötig)
>
>  


# Konzept

s. EAMod:

Gitlab Projekte sollten nur nach einer bestimmten - noch zu definierenden - Archivierungsfrist gelöscht werden.

Da Gitlab das Löschen nach einer Archivierungsfrist nicht unterstützt, wird die Logik in Ansible Workflows umgesetzt, die entsprechende API Aufrufe vornehmen.

I) Ansible Role "Löschen eines Projektes" (kann als Clean-Up-Routine auch periodisch bspw. 2x pro Woche ausgeführt werden):

  1. Überprüfen ob das Projekt archiviert wurde

  2a. falls ja und Archivierungsfrist erreicht => Projekt wird gelöscht

  2b. falls ja und Archivierungsfrist nicht erreicht => kein Handlungsbedarf / Projekt wird nicht gelöscht.

  2c. falls nein => Projekt wird archiviert und Archivierungsfrist gesetzt.

II) Ansible Role "Löschen einer Gruppe":

  1. Überprüfen ob die Gruppe Projekte enthält

  2a. falls ja => Löschauftrag an jedes Projekt in der Gruppe, d.h. Aufruf von Ansible Role "Löschen eines Projektes"

  2b. falls nein => Gruppe wird gelöscht

## Voraussetzung

Installation der Ansible Community Collection falls nicht vorhanden:

https://github.com/ansible-collections/community.general#using-this-collection


## Quickstart


irb(main):023:0> Project.where(id: 126).first.archived?
=> true


irb(main):024:0> Project.where(id: 126).first.archived?
=> false



❯ ansible-playbook pb_gitlab_badges.yml
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost
does not match 'all'

PLAY [Retention Rules] **********************************************************************************

TASK [Gathering Facts] **********************************************************************************
ok: [localhost]

TASK [List all badges of a project] *********************************************************************
ok: [localhost]

TASK [Check if project is already marked for deletion] **************************************************
ok: [localhost] => {
    "msg": [
        {
            "id": 3,
            "image_url": "http://10.1.56.30",
            "kind": "project",
            "link_url": "http://10.1.56.30",
            "name": "To be deleted at 2023-08-01",
            "rendered_image_url": "http://10.1.56.30",
            "rendered_link_url": "http://10.1.56.30"
        }
    ]
}

TASK [Add retention period to current date] *************************************************************
changed: [localhost]

TASK [Add a badge to a project] *************************************************************************
skipping: [localhost]

PLAY RECAP **********************************************************************************************
localhost                  : ok=4    changed=1    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0



=======
# gitlab-burn-after-reading



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/StandardNerd/gitlab-burn-after-reading.git
git branch -M main
git push -uf origin main
```