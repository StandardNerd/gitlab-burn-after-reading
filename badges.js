const to_delete_at = "2023-07-29"
console.log(to_delete_at)

const { makeBadge } = require('badge-maker')
const fs = require('fs')

const svgToDeleteAt = makeBadge({
  label: 'to delete at',
  message: to_delete_at,
  color: 'red',
})

fs.mkdir('badges', err => {
  if (err && err.errno !== -17) console.error(err)
})

fs.writeFile('badges/to_delete_at.svg', svgToDeleteAt, err => {
  if (err) console.error(err);
})

